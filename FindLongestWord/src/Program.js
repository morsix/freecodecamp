function Program() {}

Program.prototype.myFunc = function(str) {
    var length = 0;
    str.split(" ").forEach(function(element) {
        if (element.length > length) {
            length = element.length;
        }
    }, this);
    return length;
};

// Return the length of the longest word in the provided sentence.

// Your response should be a number.