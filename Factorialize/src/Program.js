function Program() {}

Program.prototype.myFunc = function(num) {
    var result = 1;
    for (var index = 2; index <= num; index++) {
        result = result * index;
    }
    return result;
};