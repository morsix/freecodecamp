function Program() {}

Program.prototype.myFunc = function(str) {
    var letterNumber = /^[0-9a-zA-Z]+$/;

    var parsedChars = [];
    str.toLowerCase().split("").forEach(function(element) {
        if (element.match(letterNumber)) {
            parsedChars.push(element);
        }
    }, this);

    if (parsedChars.join() === parsedChars.reverse().join()) {
        return true;
    }
    return false;
};

// Return true if the given string is a palindrome. Otherwise, return false.

// A palindrome is a word or sentence that's spelled the same way both forward and backward, ignoring punctuation, case, and spacing.

// Note
// You'll need to remove all non-alphanumeric characters (punctuation, spaces and symbols) and turn everything lower case in order to check for palindromes.