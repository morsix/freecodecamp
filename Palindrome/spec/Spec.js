describe("Program", function() {

    it("should be equal", function() {

        var program = new Program();

        // Write your testcases here 
        expect(program.myFunc('racecar')).toBe(true);
        expect(program.myFunc("2_A3*3#A2")).toBe(true);
        expect(program.myFunc("2_A3*3#A25")).toBe(false);
    });
});